# EMS1669

How to use

1.  Prepare WSEMSItem

    - (void)setupWSEMSServices {
        
        emergencyItem = [[WSEMSItemR3 alloc] init];
        [emergencyItem setInSendDateTimeFromDate:[NSDate date]];
        [emergencyItem setInLat:13.789147f inLng:100.591939f];
        emergencyItem.inPhoneNumber = @"0837999739";
        emergencyItem.inProvince = @"50";
        emergencyItem.inOtherDetail = @"Test from developer";
        emergencyItem.inCallerName = @"Test from developer";
        emergencyItem.inSelfTriage = @"Test from developer";
    }


2.  Determine RequestClass or TestClass

    - (Class)odm_WSEMSCallRequestClass {
        
        if ([self isEnableTestMode]) {
            return [WSEMSCallTestRequest class];
        } else {
            return [WSEMSCallRequest class];
        }
    }


3.  Perform POST method from WSEMSCallOperation block

    - (void)odm_postEmergencyRequest {
        
        Class WSEMSRequestClass = [self odm_WSEMSCallRequestClass];
        id request = [WSEMSRequestClass createRequestWithItem:emergencyItem];
        [WSEMSCallOperation
         postEmergencyRequest:request
         completionBlock:^{
             [self odm_completionAction];
         }
         failedBlock:^{
             [self odm_failedAction];
         }];
    }