Pod::Spec.new do |s|

  s.name         = "wsemscall"
  s.version      = "0.0.1"
  s.summary      = "SOAP API lib for 1669."
  s.homepage     = "https://bitbucket.org/opendream/wsemscall"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Developer Opendream" => "dev@opendream.co.th" }
  s.source       = { :git => "https://bitbucket.org/opendream/wsemscall.git", :tag => "0.1.5"}


  # s.public_header_files = "WSEMSCall/*.h"
  s.source_files  = "WSEMSCall/*.h"

  s.subspec 'Class' do |ss|
    ss.source_files = 'AFNetworking/Class/*.{h,m}'
    # ss.ios.frameworks = 'Class'
  end

end
