//
//  WSEMSCallRequest.m
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSCallRequest.h"
#import "WSEMSConstant.h"
#import "WSEMSItem.h"

@interface WSEMSCallRequest ()
@property (strong, nonatomic) WSEMSItem *item;
@end

@implementation WSEMSCallRequest

+ (instancetype)createRequestWithItem:(WSEMSItem *)item {
    
    NSURL *serviceURL = [NSURL URLWithString:WSEMSServicePath];
    NSString *body = [[self class] buildBodyMessage:item];
    NSString *length = [NSString stringWithFormat:@"%tu", body.length];
    
    WSEMSCallRequest *request = [[self class] requestWithURL:serviceURL];
    [request setHTTPMethod:WSEMSServiceMethod];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:WSEMSRequestTimeoutInteval];
    [request addValue:WSEMSServiceContentType forHTTPHeaderField:@"Content-Type"];
    [request addValue:WSEMSServiceHost forHTTPHeaderField:@"Host"];
    [request addValue:[request SOAPAction] forHTTPHeaderField:@"SOAPAction"];
    [request addValue:WSEMSServiceUserAgent forHTTPHeaderField:@"User-Agent"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    return request;
}

+ (NSString *)buildBodyMessage:(WSEMSItem *)item {
    
    NSString *requestTemplate = [[self class] templateStringFormatter];
    NSString *header = [NSString stringWithFormat:requestTemplate,
                        WSEMSServiceUsername
                        , WSEMSServicePassword
                        , item.inSendDateTime
                        , item.inPhoneNumber
                        , item.inProvince
                        , item.inLat
                        , item.inLng
                        , item.inSelfTriage
                        , item.inOtherDetail
                        , item.inCallerName];
    return header;
}

- (NSString *)description {
    
    NSString *body = [[NSString alloc]
                      initWithData:self.HTTPBody
                      encoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:
            @"\n-------%@-------\n%@\n-------Body-------\n%@\n"
            , NSStringFromClass([self class])
            , self.URL
            , body];
}

@end

@implementation WSEMSCallRequest (WSEMSConfiguration)

- (NSString *)SOAPAction {
    
    return WSEMSServiceAction;
}

+ (NSString *)templateStringFormatter {
    
    NSURL *templateURL = [[self class] templateURL];
    NSString *formatter =  [NSString stringWithContentsOfURL:templateURL
                                                    encoding:NSUTF8StringEncoding
                                                       error:NULL];
    return formatter;
}

+ (NSURL *)templateURL {
    
    NSString *templateName = [[self class] templateName];
    NSURL *url = [[NSBundle mainBundle]
                  URLForResource:templateName
                  withExtension:@"xml"];
    return url;
}

+ (NSString *)templateName {
    
    return WSEMSCallRequestTemplateR3;
}

@end