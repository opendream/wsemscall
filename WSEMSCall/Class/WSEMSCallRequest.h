//
//  WSEMSCallRequest.h
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

@class WSEMSItem;
@interface WSEMSCallRequest : NSMutableURLRequest
+ (instancetype)createRequestWithItem:(WSEMSItem *)item;
@end

@interface WSEMSCallRequest (WSEMSConfiguration)
- (NSString *)SOAPAction;
+ (NSString *)templateStringFormatter;
+ (NSString *)templateName;
@end
