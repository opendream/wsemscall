//
//  WSEMSCallTestRequest.m
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSCallTestRequest.h"
#import "WSEMSConstant.h"

@implementation WSEMSCallTestRequest

+ (NSString *)buildBodyMessage:(WSEMSItem *)item {
    
    NSString *requestTemplate = [[self class] templateStringFormatter];
    NSString *header = [NSString stringWithFormat:requestTemplate
                        , WSEMSServiceUsername
                        , WSEMSServicePassword];
    return header;
}

@end

@implementation WSEMSCallTestRequest (WSEMSConfiguration)

- (NSString *)SOAPAction {
    
    return WSEMSServiceTestAction;
}

+ (NSString *)templateName {
    
    return WSEMSCallTestRequestTemplateR3;
}

@end