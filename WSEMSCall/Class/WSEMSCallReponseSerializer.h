//
//  WSEMSCallReponseSerializer.h
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSConstant.h"

@interface WSEMSCallReponseSerializer : NSObject
@property (strong, nonatomic, readonly) NSData *data;

+ (void)parseData:(NSData *)responseData
  completionBlock:(WSEMSResponseBlock)completionBlock
      failedBlock:(WSEMSResponseBlock)failedBlock;
@end
