//
//  WSEMSItemR3.m
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSItemR3.h"

@implementation WSEMSItemR3

- (void)setInLat:(WSEMSLocationDegrees)lat
           inLng:(WSEMSLocationDegrees)lng {
    
    self.inLat = [NSString stringWithFormat:@"%f", lat];
    self.inLng = [NSString stringWithFormat:@"%f", lng];
}

- (void)setInSendDateTimeFromDate:(NSDate *)inSendDateTime {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [gregorianCalendar components:
                              NSYearCalendarUnit
                              | NSMonthCalendarUnit
                              | NSDayCalendarUnit
                              | NSHourCalendarUnit
                              | NSMinuteCalendarUnit
                              | NSSecondCalendarUnit fromDate:inSendDateTime];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:WSEMSCallItemDateTimeFormatter];
    self.inSendDateTime = [dateFormatter stringFromDate:[gregorianCalendar dateFromComponents:comp]];
}

@end
