//
//  WSEMSCallOperation.m
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSCallOperation.h"
#import "WSEMSCallReponseSerializer.h"

@interface WSEMSCallOperation () <NSXMLParserDelegate>
@end

@implementation WSEMSCallOperation

- (instancetype)initWithRequest:(WSEMSCallRequest *)request {
    
    if (self = [super init]) {
        _request = request;
    }
    return self;
}

+ (void)postEmergencyRequest:(WSEMSCallRequest *)request
             completionBlock:(void (^)())completionBlock
                 failedBlock:(void (^)())failedBlock {
    
    id operation = [[[self class] alloc] initWithRequest:request];
    [operation setCompletionBlock:completionBlock];
    [operation setFailedBlock:failedBlock];
    
    if ([operation request]) {
        [operation odm_postEmergencyRequest];
    } else {
        [operation odm_performFailedBlock];
    }
}

- (void)odm_postEmergencyRequest {
    
    // DDLogVerbose(@"\n-------POST EMERGENCY-------\n%@\n", self.request);
    [NSURLConnection
     sendAsynchronousRequest:self.request
     queue:[NSOperationQueue mainQueue]
     completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         NSString *responseString = [[NSString alloc]
                                     initWithData:data
                                     encoding:NSUTF8StringEncoding];
         // DDLogVerbose(@"\n-------Response Data-------\n%@\n"
                       // , responseString);
         [WSEMSCallReponseSerializer
          parseData:data
          completionBlock:^{
              [self odm_performCompletionBlock];
          }
          failedBlock:^{
              [self odm_performFailedBlock];
          }];
     }];
}

- (void)odm_performCompletionBlock {
    
    if (self.completionBlock) {
        self.completionBlock();
    }
}

- (void)odm_performFailedBlock {
    
    if (self.failedBlock) {
        self.failedBlock();
    }
}

@end
