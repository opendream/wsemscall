//
//  WSEMSItem.h
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSEMSItem : NSObject
@property (copy, nonatomic) NSString *inPhoneNumber;
@property (copy, nonatomic) NSString *inProvince;
@property (copy, nonatomic) NSString *inLat;
@property (copy, nonatomic) NSString *inLng;
@property (copy, nonatomic) NSString *inSelfTriage;
@property (copy, nonatomic) NSString *inOtherDetail;
@property (copy, nonatomic) NSString *inCallerName;
@property (copy, nonatomic) NSString *inSendDateTime;
@end