//
//  WSEMSCallOperation.h
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSCallTestRequest.h"
#import "WSEMSCallRequest.h"
#import "WSEMSConstant.h"

@interface WSEMSCallOperation : NSObject
@property (strong, nonatomic) WSEMSCallRequest *request;
@property (strong,nonatomic) WSEMSResponseBlock completionBlock;
@property (strong,nonatomic) WSEMSResponseBlock failedBlock;

- (instancetype)initWithRequest:(WSEMSCallRequest *)request;
+ (void)postEmergencyRequest:(WSEMSCallRequest *)request
             completionBlock:(void (^)())completionBlock
                 failedBlock:(void (^)())failedBlock;
@end
