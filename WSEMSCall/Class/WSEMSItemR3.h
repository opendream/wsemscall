//
//  WSEMSItemR3.h
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSItem.h"
#import "WSEMSConstant.h"

@interface WSEMSItemR3 : WSEMSItem
- (void)setInLat:(WSEMSLocationDegrees)lat
           inLng:(WSEMSLocationDegrees)lng;
- (void)setInSendDateTimeFromDate:(NSDate *)inSendDateTime;
@end
