//
//  WSEMSCallReponseSerializer.m
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

#import "WSEMSCallReponseSerializer.h"

static NSString * const WSEMSCallResponseTestElement = @"SendEMSCallTestResult";
static NSString * const WSEMSCallResponseActionName = @"SendEMSCall_R3";
static NSString * const WSEMSCallResultNode = @"SendEMSCall_R3Result";

@interface WSEMSCallReponseSerializer () <NSXMLParserDelegate> {
    BOOL isStoringResult;
    NSMutableString *result;
}

@property (strong, nonatomic) WSEMSResponseBlock completionBlock;
@property (strong, nonatomic) WSEMSResponseBlock failedBlock;
@property (strong, nonatomic, readwrite) NSData *data;
@end

@interface WSEMSCallReponseSerializer (ResponseAction)
- (void)odm_performCompletionBlock:(id)sender;
- (void)odm_performFailedBlock:(id)sender;
@end

@implementation WSEMSCallReponseSerializer

- (instancetype)initWithData:(NSData *)data
             completionBlock:(WSEMSResponseBlock)completionBlock
                 failedBlock:(WSEMSResponseBlock)failedBlock {
    
    if (self = [super init]) {
        _completionBlock = completionBlock;
        _failedBlock = failedBlock;
        _data = data;
    }
    return self;
}

+ (void)parseData:(NSData *)responseData
  completionBlock:(WSEMSResponseBlock)completionBlock
      failedBlock:(WSEMSResponseBlock)failedBlock {
    
    id responseSerializer = [[[self class] alloc]
                             initWithData:responseData
                             completionBlock:completionBlock
                             failedBlock:failedBlock];
    
    if (![responseSerializer parse]) {
        [responseSerializer odm_performFailedBlock:responseSerializer];
    }
}

- (BOOL)parse {
    
    if (self.data) {
        NSXMLParser *parser = [[NSXMLParser alloc] initWithData:self.data];
        [parser setDelegate:self];
        return [parser parse];
    } else {
        return NO;
    }
}

#pragma mark - xml parser delegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:WSEMSCallResponseTestElement]
        || [elementName isEqualToString:WSEMSCallResultNode]) {
        result = [NSMutableString new];
        isStoringResult = YES;
    }
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:WSEMSCallResponseTestElement]
        || [elementName isEqualToString:WSEMSCallResultNode]) {
        isStoringResult = NO;
        if ([result isEqualToString:@"1"]) {
            [self odm_performCompletionBlock:parser];
        } else {
            [self odm_performFailedBlock:parser];
        }
    }
}

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string {
    
    if (isStoringResult) {
        [result appendString:string];
    }
}

@end

@implementation WSEMSCallReponseSerializer (ResponseAction)

- (void)odm_performCompletionBlock:(id)sender {
    
    if (self.completionBlock) {
        self.completionBlock();
    }
}

- (void)odm_performFailedBlock:(id)sender {
    
    if (self.failedBlock) {
        self.failedBlock();
    }
}

@end