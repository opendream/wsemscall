//
//  WSEMSConstant.h
//  DoctorMe
//
//  Created by Opendream-iOS on 4/7/2557 BE.
//  Copyright (c) 2557 Opendream. All rights reserved.
//

typedef double WSEMSLocationDegrees;

typedef void(^WSEMSResponseBlock)();

static NSString * const WSEMSServiceHost = @"ws.niems.go.th";
static NSString * const WSEMSServicePath = @"http://ws.niems.go.th/ws_emscall/ws_ems_call.asmx";
static NSString * const WSEMSServiceUsername = @"doctorme";
static NSString * const WSEMSServicePassword = @"niemitems";
static NSString * const WSEMSServiceMethod = @"POST";
static NSString * const WSEMSServiceAction = @"http://tempuri.org/SendEMSCall_R3";
static NSString * const WSEMSServiceTestAction = @"http://tempuri.org/SendEMSCallTest";
static NSString * const WSEMSServiceUserAgent = @"doctorme";
static NSString * const WSEMSServiceContentType = @"application/soap+xml; charset=utf-8";

static NSString * const WSEMSCallRequestTemplateR3 = @"WSEMSCallRequestTemplateR3";
static NSString * const WSEMSCallTestRequestTemplateR3 = @"WSEMSCallTestRequestTemplateR3";

static NSString * const WSEMSCallItemDateTimeFormatter = @"yyyy-MM-dd HH:mm:ss";
static const NSUInteger WSEMSRequestTimeoutInteval = 5;